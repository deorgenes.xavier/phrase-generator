package com.example.phrasegenerator.repository;

import com.example.phrasegenerator.entity.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ParameterRepository extends JpaRepository<Parameter, Long> {
	Optional<Parameter> findByNameAndType(String name, String type);
	List<Parameter> findAllByType(String type);
}
