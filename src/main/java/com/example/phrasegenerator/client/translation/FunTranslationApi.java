package com.example.phrasegenerator.client.translation;

public interface FunTranslationApi {
	String retrievePhraseByCharacter(String text, String character);
}
