package com.example.phrasegenerator.client.translation;

import com.example.phrasegenerator.client.translation.dto.TranslateRequestDto;
import com.example.phrasegenerator.client.translation.dto.TranslateResponseDto;
import com.example.phrasegenerator.exception.types.FunTranslationApiException;
import com.example.phrasegenerator.service.ParameterService;
import com.example.phrasegenerator.util.Validator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.Charset;

import static com.example.phrasegenerator.util.ConstantUtil.FUN_TRANSLATION_API_NAME;
import static com.example.phrasegenerator.util.ConstantUtil.HEADER_CONTENT_TYPE;

@Slf4j
@Service
@RequiredArgsConstructor
public class FunTranslationApiService implements FunTranslationApi {

	private final RestTemplate restTemplate;
	private final ParameterService parameterService;

	@Override
	public final String retrievePhraseByCharacter(final String text, final String character) {
		final String baseUrl = parameterService.retrieveUrlByName(FUN_TRANSLATION_API_NAME);
		final String yodaEndpoint = parameterService.retrieveCharacterByName(character);
		final String url = baseUrl + yodaEndpoint;

		final UriComponentsBuilder builder = UriComponentsBuilder
			.fromHttpUrl(url);

		final HttpHeaders headers = new HttpHeaders();
		headers.set(HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		final TranslateRequestDto translateRequestDto = new TranslateRequestDto(text);

		try {
			final ResponseEntity<TranslateResponseDto> responseEntity = this.restTemplate.exchange(
				builder.build().encode(Charset.defaultCharset()).toUri(),
				HttpMethod.POST,
				new HttpEntity<>(translateRequestDto, headers),
				TranslateResponseDto.class
			);

			Validator.validateResponseEntity(responseEntity);

			return responseEntity.getBody()
				.getContents()
				.getTranslated();
		} catch (Exception ex) {
			log.error("Error with message: {}", ex.getMessage());
			throw new FunTranslationApiException();
		}
	}
}
