package com.example.phrasegenerator.client.translation.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TranslateContents {
	private String translated;
	private String text;
	private String translation;
}
