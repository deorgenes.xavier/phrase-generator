package com.example.phrasegenerator.client.translation.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TranslateRequestDto {
	private String text;
}
