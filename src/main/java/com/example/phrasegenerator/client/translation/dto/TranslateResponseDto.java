package com.example.phrasegenerator.client.translation.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TranslateResponseDto {
	private TranslateSuccess success;
	private TranslateContents contents;
}
