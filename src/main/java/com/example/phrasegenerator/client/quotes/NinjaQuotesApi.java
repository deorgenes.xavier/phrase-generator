package com.example.phrasegenerator.client.quotes;

import com.example.phrasegenerator.client.quotes.dto.QuoteResponseDto;

public interface NinjaQuotesApi {
	String retrieveQuoteByCategory(String category);
	QuoteResponseDto retrieveRandomQuote();
}
