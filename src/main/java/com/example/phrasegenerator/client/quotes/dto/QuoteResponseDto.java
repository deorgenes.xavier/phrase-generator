package com.example.phrasegenerator.client.quotes.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuoteResponseDto {
	private String quote;
	private String author;
	private String category;
}
