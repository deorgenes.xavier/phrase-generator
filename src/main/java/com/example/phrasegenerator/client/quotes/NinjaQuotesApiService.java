package com.example.phrasegenerator.client.quotes;

import com.example.phrasegenerator.client.quotes.dto.QuoteResponseDto;
import com.example.phrasegenerator.exception.types.NinjaQuotesApiException;
import com.example.phrasegenerator.service.ParameterService;
import com.example.phrasegenerator.util.Validator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.List;

import static com.example.phrasegenerator.util.ConstantUtil.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class NinjaQuotesApiService implements NinjaQuotesApi {

	private final RestTemplate restTemplate;
	private final ParameterService parameterService;

	@Override
	public String retrieveQuoteByCategory(final String category) {
		try {
			final ResponseEntity<List<QuoteResponseDto>> responseEntity = this.restTemplate.exchange(
				buildUriWithParams(PARAM_CATEGORY, category),
				HttpMethod.GET,
				new HttpEntity<>(null, buildHeader()),
				new ParameterizedTypeReference<>() {
				}
			);

			Validator.validateResponseEntity(responseEntity);

			return responseEntity.getBody()
				.stream()
				.findAny()
				.orElseThrow(() -> new RuntimeException("no quotes were found"))
				.getQuote();

		} catch (Exception ex) {
			log.info("Error generating quote with category {}:  {}", category, ex.getMessage());
			throw new NinjaQuotesApiException();
		}
	}

	@Override
	public QuoteResponseDto retrieveRandomQuote() {
		try {
			final ResponseEntity<List<QuoteResponseDto>> responseEntity = this.restTemplate.exchange(
				buildUriWithoutParams(),
				HttpMethod.GET,
				new HttpEntity<>(null, buildHeader()),
				new ParameterizedTypeReference<>() {
				}
			);

			Validator.validateResponseEntity(responseEntity);

			return responseEntity.getBody()
				.stream()
				.findAny()
				.orElseThrow(() -> new RuntimeException("no quotes were found"));

		} catch (Exception ex) {
			log.info("Error generating random quote: {}", ex.getMessage());
			throw new NinjaQuotesApiException();
		}
	}

	private URI buildUriWithParams(final String key, final String value) {
		final MultiValueMap<String, String> queryParameters = new LinkedMultiValueMap<>();
		queryParameters.add(key, value);

		return buildUri(queryParameters);
	}

	private URI buildUriWithoutParams() {
		return buildUri(null);
	}

	private URI buildUri(final MultiValueMap<String, String> queryParameters) {
		final String url = parameterService.retrieveUrlByName(NINJA_QUOTE_API_NAME);

		return UriComponentsBuilder.fromHttpUrl(url)
			.queryParams(queryParameters)
			.build()
			.encode(Charset.defaultCharset())
			.toUri();
	}

	private HttpHeaders buildHeader() {
		final String key = parameterService.retrieveKeyByName(NINJA_QUOTE_API_NAME);

		final HttpHeaders headers = new HttpHeaders();
		headers.set(HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.set(HEADER_X_API_KEY, key);
		return headers;
	}
}
