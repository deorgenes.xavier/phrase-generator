package com.example.phrasegenerator.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PHRASE_TABLE")
public class Phrase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String text;
	private String character;
	private String category;
	private LocalDate createdAt;
	private LocalDate updatedAt;
}
