package com.example.phrasegenerator.service;

import com.example.phrasegenerator.entity.Parameter;
import com.example.phrasegenerator.enums.ParameterType;
import com.example.phrasegenerator.exception.types.*;
import com.example.phrasegenerator.repository.ParameterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.phrasegenerator.enums.ParameterType.*;

@Service
@RequiredArgsConstructor
public class ParameterService {
	private final ParameterRepository parameterRepository;

	public String retrieveUrlByName(final String name) {
		return parameterRepository.findByNameAndType(name, URL.getDescription())
			.orElseThrow(UrlNotFoundException::new)
			.getContent();
	}

	public String retrieveCharacterByName(final String name) {
		return parameterRepository.findByNameAndType(name, CHARACTER.getDescription())
			.orElseThrow(CharacterNotFoundException::new)
			.getContent();
	}

	public String retrieveKeyByName(final String name) {
		return parameterRepository.findByNameAndType(name, KEY.getDescription())
			.orElseThrow(KeyNotFoundException::new)
			.getContent();
	}

	public List<String> retrieveAllFunnyCharacters() {
		return retrieveAllByParameterType(CHARACTER);
	}

	public List<String> retrieveAllQuoteCategories() {
		return retrieveAllByParameterType(CATEGORY);
	}

	private List<String> retrieveAllByParameterType(final ParameterType type) {
		return parameterRepository.findAllByType(type.getDescription())
			.stream()
			.map(Parameter::getName)
			.toList();
	}
}
