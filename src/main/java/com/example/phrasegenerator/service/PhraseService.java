package com.example.phrasegenerator.service;

import com.example.phrasegenerator.client.quotes.NinjaQuotesApi;
import com.example.phrasegenerator.client.quotes.dto.QuoteResponseDto;
import com.example.phrasegenerator.client.translation.FunTranslationApi;
import com.example.phrasegenerator.entity.Phrase;
import com.example.phrasegenerator.repository.PhraseRepository;
import com.example.phrasegenerator.util.Validator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PhraseService {

	private final FunTranslationApi funTranslationApi;
	private final NinjaQuotesApi ninjaQuotesApi;
	private final ParameterService parameterService;
	private final PhraseRepository phraseRepository;

	public String retrieveFunnyPhraseByParams(final Phrase phrase) {
		final String quote = resolveQuote(phrase);
		final String finalQuote = resolveTranslationByCharacter(phrase.getCharacter(), quote);

		phraseRepository.save(new Phrase(
			null,
			finalQuote,
			phrase.getCharacter(),
			phrase.getCategory(),
			LocalDate.now(),
			LocalDate.now()
		));

		return finalQuote;
	}

	public List<String> retrieveAllFunnyCharacters() {
		return parameterService.retrieveAllFunnyCharacters();
	}

	public List<String> retrieveAllQuoteCategories() {
		return parameterService.retrieveAllQuoteCategories();
	}

	private String resolveQuote(final Phrase phrase){
		if (Validator.isNullOrEmpty(phrase.getText())) {
			return resolveExternalQuoteByCategory(phrase.getCategory());
		}

		return phrase.getText();
	}

	private String resolveExternalQuoteByCategory(final String phraseCategory) {
		if (Validator.isNullOrEmpty(phraseCategory)) {
			return ninjaQuotesApi.retrieveRandomQuote().getQuote();
		}

		return  ninjaQuotesApi.retrieveQuoteByCategory(phraseCategory);
	}

	private String resolveTranslationByCharacter(final String phraseCharacter, final String phraseQuote){
		if(Validator.isNullOrEmpty(phraseCharacter)){
			return phraseQuote;
		}

		return funTranslationApi.retrievePhraseByCharacter(phraseQuote, phraseCharacter);
	}
}
