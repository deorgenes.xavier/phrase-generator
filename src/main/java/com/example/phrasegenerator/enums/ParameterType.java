package com.example.phrasegenerator.enums;

import lombok.*;

@Getter
@AllArgsConstructor
public enum ParameterType {
	KEY("key"),
	URL("url"),
	CATEGORY("category"),
	CHARACTER("character");

	private String description;
}
