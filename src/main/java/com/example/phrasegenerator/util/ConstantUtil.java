package com.example.phrasegenerator.util;

public class ConstantUtil {
	public static final String RESPONSE_PHRASE = "phrase";
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HEADER_X_API_KEY = "X-Api-Key";
	public static final String PARAM_CATEGORY = "category";
	public static final String FUN_TRANSLATION_API_NAME = "fun_translation_api";
	public static final String NINJA_QUOTE_API_NAME = "ninja_quotes_api";
}
