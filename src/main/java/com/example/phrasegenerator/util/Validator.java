package com.example.phrasegenerator.util;

import org.springframework.http.ResponseEntity;

import java.util.List;

public class Validator {

	public static boolean isNullOrEmpty(final String str) {
		return str == null || str.trim().isEmpty();
	}

	public static <T> void validateResponseEntity(final ResponseEntity<T> responseEntity){
		if(responseEntity.getStatusCode().is3xxRedirection()){
			throw new RuntimeException("Redirection error occurred with status: " + responseEntity.getStatusCode().value());
		}

		if(responseEntity.getStatusCode().is4xxClientError()){
			throw new RuntimeException("Client error occurred with status: " + responseEntity.getStatusCode().value());
		}

		if(responseEntity.getStatusCode().is5xxServerError()){
			throw new RuntimeException("Server error occurred with status: " + responseEntity.getStatusCode().value());
		}

		if(responseEntity.getBody() == null){
			throw new RuntimeException("Client returned null body");
		}

		if(responseEntity.getBody() instanceof List && ((List<?>) responseEntity.getBody()).isEmpty()){
			throw new RuntimeException("Client returned empty body");
		}
	}
}
