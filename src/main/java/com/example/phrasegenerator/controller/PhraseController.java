package com.example.phrasegenerator.controller;

import com.example.phrasegenerator.dto.PhraseRequestDto;
import com.example.phrasegenerator.service.PhraseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static com.example.phrasegenerator.util.ConstantUtil.RESPONSE_PHRASE;

@RestController
@RequestMapping("/api/v1/phrases")
@RequiredArgsConstructor
public class PhraseController {

	private final PhraseService service;

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, String>> createPhrase(@RequestBody final PhraseRequestDto phraseRequestDto) {
		return ResponseEntity.ok(Map.of(
			RESPONSE_PHRASE,
			service.retrieveFunnyPhraseByParams(phraseRequestDto.toPhrase(LocalDate.now()))
		));
	}

	@GetMapping(value = "/characters", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> retrieveAllCharacters() {
		return ResponseEntity.ok(service.retrieveAllFunnyCharacters());
	}

	@GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> retrieveAllQuoteCategories() {
		return ResponseEntity.ok(service.retrieveAllQuoteCategories());
	}
}
