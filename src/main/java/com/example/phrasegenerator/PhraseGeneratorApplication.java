package com.example.phrasegenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhraseGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhraseGeneratorApplication.class, args);
	}

}
