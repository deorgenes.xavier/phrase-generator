package com.example.phrasegenerator.dto;

import com.example.phrasegenerator.entity.Phrase;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class PhraseRequestDto {

	private String text;
	private String character;
	private String category;

	public Phrase toPhrase(final LocalDate createdAt){
		return new Phrase(null, this.text, this.character, this.category, createdAt, createdAt);
	}
}
