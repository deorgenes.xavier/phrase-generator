package com.example.phrasegenerator.exception;

import java.util.Date;

public record ErrorDetails(Date timestamp, String message) {
}
