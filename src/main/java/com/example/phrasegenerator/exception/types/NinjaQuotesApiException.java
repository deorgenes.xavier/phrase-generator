package com.example.phrasegenerator.exception.types;

public class NinjaQuotesApiException extends RuntimeException {

    public NinjaQuotesApiException() {
        super("Error occurred when trying to call quotes client");
    }

}
