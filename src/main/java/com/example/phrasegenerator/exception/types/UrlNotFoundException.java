package com.example.phrasegenerator.exception.types;

public class UrlNotFoundException extends RuntimeException {

    public UrlNotFoundException() {
        super("URL not found");
    }

}
