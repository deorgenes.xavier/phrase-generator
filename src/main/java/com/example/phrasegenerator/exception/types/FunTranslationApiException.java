package com.example.phrasegenerator.exception.types;

public class FunTranslationApiException extends RuntimeException {

    public FunTranslationApiException() {
        super("Error occurred when trying to call Translation client");
    }

}
