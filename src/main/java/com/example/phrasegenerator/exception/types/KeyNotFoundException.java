package com.example.phrasegenerator.exception.types;

public class KeyNotFoundException extends RuntimeException {

    public KeyNotFoundException() {
        super("Key not found");
    }

}
