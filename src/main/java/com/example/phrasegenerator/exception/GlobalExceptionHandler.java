package com.example.phrasegenerator.exception;


import com.example.phrasegenerator.exception.types.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Date;

@RestControllerAdvice
public class GlobalExceptionHandler {

	private static ResponseEntity<ErrorDetails> buildErrorResponse(final HttpStatus status, final Exception e) {
		return ResponseEntity.status(status)
			.body(new ErrorDetails(new Date(), e.getMessage()));
	}

	@ExceptionHandler(UrlNotFoundException.class)
	public ResponseEntity<ErrorDetails> handle(final UrlNotFoundException e) {
		return buildErrorResponse(HttpStatus.NOT_FOUND, e);
	}

	@ExceptionHandler(CharacterNotFoundException.class)
	public ResponseEntity<ErrorDetails> handle(final CharacterNotFoundException e) {
		return buildErrorResponse(HttpStatus.NOT_FOUND, e);
	}

	@ExceptionHandler(KeyNotFoundException.class)
	public ResponseEntity<ErrorDetails> handle(final KeyNotFoundException e) {
		return buildErrorResponse(HttpStatus.NOT_FOUND, e);
	}

	@ExceptionHandler(FunTranslationApiException.class)
	public ResponseEntity<ErrorDetails> handle(final FunTranslationApiException e) {
		return buildErrorResponse(HttpStatus.BAD_GATEWAY, e);
	}

	@ExceptionHandler(NinjaQuotesApiException.class)
	public ResponseEntity<ErrorDetails> handle(final NinjaQuotesApiException e) {
		return buildErrorResponse(HttpStatus.BAD_GATEWAY, e);
	}
}
