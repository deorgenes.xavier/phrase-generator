package com.example.phrasegenerator.client;

import com.example.phrasegenerator.client.translation.FunTranslationApiService;
import com.example.phrasegenerator.client.translation.dto.*;
import com.example.phrasegenerator.exception.types.FunTranslationApiException;
import com.example.phrasegenerator.service.ParameterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FunTranslationApiClientServiceTest {

	@Mock
	RestTemplate restTemplate;

	@Mock
	ParameterService parameterServiceMock;

	@InjectMocks
	FunTranslationApiService funTranslationApiService;

	@Test
	public void testSuccessResponse() {
		final TranslateResponseDto mockResponse =
			new TranslateResponseDto(
				new TranslateSuccess(1),
				new TranslateContents("translated", "text", "en")
			);

		lenient().when(restTemplate.exchange(any(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class)))
			.thenReturn(new ResponseEntity<>(mockResponse, HttpStatus.OK));

		when(parameterServiceMock.retrieveUrlByName(anyString()))
			.thenReturn("https://api.funtranslations.com/translate/");

		when(parameterServiceMock.retrieveCharacterByName(anyString()))
			.thenReturn("yoga");

		final String result = funTranslationApiService.retrievePhraseByCharacter("randomText", "yoga");

		Assertions.assertEquals("translated", result);

		verify(parameterServiceMock, times(1)).retrieveUrlByName(anyString());
		verify(parameterServiceMock, times(1)).retrieveCharacterByName(anyString());
	}

	@Test
	public void testErrorResponse() {
		lenient().when(restTemplate.exchange(any(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class)))
			.thenReturn(new ResponseEntity<>(null, HttpStatus.OK));

		when(parameterServiceMock.retrieveUrlByName(anyString()))
			.thenReturn("https://api.funtranslations.com/translate/");

		when(parameterServiceMock.retrieveCharacterByName(anyString()))
			.thenReturn("yoga");

		final RuntimeException exception = assertThrows(FunTranslationApiException.class, () -> {
			funTranslationApiService.retrievePhraseByCharacter("randomText", "yoga");
		});

		assertNotNull(exception);
		assertNotNull(exception.getMessage());
		assertEquals("Error occurred when trying to call Translation client", exception.getMessage());

		verify(parameterServiceMock, times(1)).retrieveUrlByName(anyString());
		verify(parameterServiceMock, times(1)).retrieveCharacterByName(anyString());
	}
}
