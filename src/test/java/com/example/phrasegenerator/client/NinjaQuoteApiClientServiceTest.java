package com.example.phrasegenerator.client;

import com.example.phrasegenerator.client.quotes.NinjaQuotesApiService;
import com.example.phrasegenerator.client.quotes.dto.QuoteResponseDto;
import com.example.phrasegenerator.exception.types.NinjaQuotesApiException;
import com.example.phrasegenerator.service.ParameterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class NinjaQuoteApiClientServiceTest {

	@Mock
	RestTemplate restTemplate;

	@Mock
	ParameterService parameterServiceMock;

	@InjectMocks
	NinjaQuotesApiService ninjaQuotesApiService;

	@Test
	public void testRetrieveRandomQuoteSuccessfully() {
		final List<QuoteResponseDto> mockResponse = List.of(new QuoteResponseDto("quote", "author", "category"));

		lenient().when(restTemplate.exchange(any(), any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class)))
			.thenReturn(new ResponseEntity<>(mockResponse, HttpStatus.OK));

		when(parameterServiceMock.retrieveUrlByName(anyString()))
			.thenReturn("https://api.api-ninjas.com/v1/quotes");

		final QuoteResponseDto result = ninjaQuotesApiService.retrieveRandomQuote();

		Assertions.assertEquals("quote", result.getQuote());
		Assertions.assertEquals("author", result.getAuthor());
		Assertions.assertEquals("category", result.getCategory());

		verify(parameterServiceMock, times(1)).retrieveUrlByName(anyString());
	}

	@Test
	public void testRetrieveQuoteByCategorySuccessfully() {
		final List<QuoteResponseDto> mockResponse = List.of(new QuoteResponseDto("quote", "author", "category"));

		lenient().when(restTemplate.exchange(any(), any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class)))
			.thenReturn(new ResponseEntity<>(mockResponse, HttpStatus.OK));

		when(parameterServiceMock.retrieveUrlByName(anyString()))
			.thenReturn("https://api.api-ninjas.com/v1/quotes");

		final String result = ninjaQuotesApiService.retrieveQuoteByCategory("category");

		Assertions.assertEquals("quote", result);

		verify(parameterServiceMock, times(1)).retrieveUrlByName(anyString());
	}

	@Test
	public void testErrorAtRetrieveRandomQuote() {
		lenient().when(restTemplate.exchange(any(), any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class)))
			.thenReturn(new ResponseEntity<>(null, HttpStatus.OK));

		when(parameterServiceMock.retrieveUrlByName(anyString()))
			.thenReturn("https://api.api-ninjas.com/v1/quotes");

		final RuntimeException exception = assertThrows(NinjaQuotesApiException.class, () -> {
			ninjaQuotesApiService.retrieveRandomQuote();
		});

		assertNotNull(exception);
		assertNotNull(exception.getMessage());
		assertEquals("Error occurred when trying to call quotes client", exception.getMessage());

		verify(parameterServiceMock, times(1)).retrieveUrlByName(anyString());
	}

	@Test
	public void testErrorAtRetrieveQuoteByCategory() {
		lenient().when(restTemplate.exchange(any(), any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class)))
			.thenReturn(new ResponseEntity<>(null, HttpStatus.OK));

		when(parameterServiceMock.retrieveUrlByName(anyString()))
			.thenReturn("https://api.api-ninjas.com/v1/quotes");

		final RuntimeException exception = assertThrows(NinjaQuotesApiException.class, () -> {
			ninjaQuotesApiService.retrieveQuoteByCategory("category");
		});

		assertNotNull(exception);
		assertNotNull(exception.getMessage());
		assertEquals("Error occurred when trying to call quotes client", exception.getMessage());

		verify(parameterServiceMock, times(1)).retrieveUrlByName(anyString());
	}
}
