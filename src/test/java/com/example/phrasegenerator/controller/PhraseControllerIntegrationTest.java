package com.example.phrasegenerator.controller;

import com.example.phrasegenerator.dto.PhraseRequestDto;
import com.example.phrasegenerator.entity.Phrase;
import com.example.phrasegenerator.exception.types.*;
import com.example.phrasegenerator.service.PhraseService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PhraseController.class)
public class PhraseControllerIntegrationTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	PhraseService phraseServiceMock;

	private static final String REQUEST_BODY = "{" +
		"\"text\":\"testAccount\"," +
		"\"character\":\"testSaxoApiKey\"," +
		"\"category\":\"testSaxoSecretKey\"" +
		"}";

	@Test
	public void testCreatePhrase() throws Exception {
		final String resultQuote = "result quote";

		when(phraseServiceMock.retrieveFunnyPhraseByParams(any(Phrase.class))).thenReturn(resultQuote);

		mockMvc.perform(post("/api/v1/phrases")
							.contentType(MediaType.APPLICATION_JSON)
							.content(REQUEST_BODY))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.phrase", is(resultQuote)));
	}

	@Test
	public void testUrlNotFound() throws Exception {
		when(phraseServiceMock.retrieveFunnyPhraseByParams(any(Phrase.class)))
			.thenThrow(new UrlNotFoundException());

		mockMvc.perform(post("/api/v1/phrases")
							.contentType(MediaType.APPLICATION_JSON)
							.content(REQUEST_BODY))
			.andExpect(status().isNotFound())
			.andExpect(jsonPath("$.message", is("URL not found")));
	}

	@Test
	public void testCharacterNotFound() throws Exception {
		when(phraseServiceMock.retrieveFunnyPhraseByParams(any(Phrase.class)))
			.thenThrow(new CharacterNotFoundException());

		mockMvc.perform(post("/api/v1/phrases")
							.contentType(MediaType.APPLICATION_JSON)
							.content(REQUEST_BODY))
			.andExpect(status().isNotFound())
			.andExpect(jsonPath("$.message", is("Character not found")));
	}

	@Test
	public void testKeyNotFound() throws Exception {
		when(phraseServiceMock.retrieveFunnyPhraseByParams(any(Phrase.class)))
			.thenThrow(new KeyNotFoundException());

		mockMvc.perform(post("/api/v1/phrases")
							.contentType(MediaType.APPLICATION_JSON)
							.content(REQUEST_BODY))
			.andExpect(status().isNotFound())
			.andExpect(jsonPath("$.message", is("Key not found")));
	}

	@Test
	public void testFunTranslationApiError() throws Exception {
		when(phraseServiceMock.retrieveFunnyPhraseByParams(any(Phrase.class)))
			.thenThrow(new FunTranslationApiException());

		mockMvc.perform(post("/api/v1/phrases")
							.contentType(MediaType.APPLICATION_JSON)
							.content(REQUEST_BODY))
			.andExpect(status().isBadGateway())
			.andExpect(jsonPath("$.message", is("Error occurred when trying to call Translation client")));
	}

	@Test
	public void testNinjaQuoteApiError() throws Exception {
		when(phraseServiceMock.retrieveFunnyPhraseByParams(any(Phrase.class)))
			.thenThrow(new NinjaQuotesApiException());

		mockMvc.perform(post("/api/v1/phrases")
							.contentType(MediaType.APPLICATION_JSON)
							.content(REQUEST_BODY))
			.andExpect(status().isBadGateway())
			.andExpect(jsonPath("$.message", is("Error occurred when trying to call quotes client")));
	}

	@Test
	public void testReturnCharacters() throws Exception {
		when(phraseServiceMock.retrieveAllFunnyCharacters()).thenReturn(List.of(
			"yoda",
			"sith",
			"uk2us"
		));

		mockMvc.perform(get("/api/v1/phrases/characters")
							.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$", hasSize(3)))
			.andExpect(jsonPath("$[0]", is("yoda")))
			.andExpect(jsonPath("$[1]", is("sith")))
			.andExpect(jsonPath("$[2]", is("uk2us")));
	}

	@Test
	public void testReturnEmptyCharacters() throws Exception {
		when(phraseServiceMock.retrieveAllFunnyCharacters()).thenReturn(List.of());

		mockMvc.perform(get("/api/v1/phrases/characters")
							.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$", hasSize(0)));
	}

	@Test
	public void testReturnQuotes() throws Exception {
		when(phraseServiceMock.retrieveAllQuoteCategories()).thenReturn(List.of(
			"god",
			"freedom",
			"happiness"
		));

		mockMvc.perform(get("/api/v1/phrases/categories")
							.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$", hasSize(3)))
			.andExpect(jsonPath("$[0]", is("god")))
			.andExpect(jsonPath("$[1]", is("freedom")))
			.andExpect(jsonPath("$[2]", is("happiness")));
	}

	@Test
	public void testReturnEmptyQuotes() throws Exception {
		when(phraseServiceMock.retrieveAllQuoteCategories()).thenReturn(List.of());

		mockMvc.perform(get("/api/v1/phrases/categories")
							.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$", hasSize(0)));
	}
}