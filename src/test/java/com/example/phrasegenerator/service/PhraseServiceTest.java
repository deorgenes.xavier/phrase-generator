package com.example.phrasegenerator.service;


import com.example.phrasegenerator.client.quotes.NinjaQuotesApi;
import com.example.phrasegenerator.client.quotes.dto.QuoteResponseDto;
import com.example.phrasegenerator.client.translation.FunTranslationApi;
import com.example.phrasegenerator.entity.Phrase;
import com.example.phrasegenerator.repository.PhraseRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PhraseServiceTest {

	@Mock
	FunTranslationApi funTranslationApi;

	@Mock
	NinjaQuotesApi ninjaQuotesApi;

	@Mock
	ParameterService parameterService;

	@Mock
	PhraseRepository phraseRepository;

	@InjectMocks
	PhraseService phraseService;

	@Test
	public void testRetrieveFunnyPhraseWithOnlyText() {
		final Phrase phrase = new Phrase(
			1L,
			"rawText",
			"",
			"",
			LocalDate.of(2024, 01, 01),
			LocalDate.of(2024, 01, 01)
		);

		final String result = phraseService.retrieveFunnyPhraseByParams(phrase);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals("rawText", result);

		verify(phraseRepository, times(1)).save(any(Phrase.class));
	}

	@Test
	public void testRetrieveFunnyPhraseWithTextAndCharacter() {
		final Phrase phrase = new Phrase(
			1L,
			"rawText",
			"yoda",
			"",
			LocalDate.of(2024, 01, 01),
			LocalDate.of(2024, 01, 01)
		);

		when(funTranslationApi.retrievePhraseByCharacter(anyString(), anyString()))
			.thenReturn("yodaText");

		final String result = phraseService.retrieveFunnyPhraseByParams(phrase);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals("yodaText", result);

		verify(phraseRepository, times(1)).save(any(Phrase.class));
		verify(funTranslationApi, times(1)).retrievePhraseByCharacter(anyString(), anyString());
	}

	@Test
	public void testRetrieveFunnyPhraseWithOnlyCategory() {
		final Phrase phrase = new Phrase(
			1L,
			"",
			"",
			"happiness",
			LocalDate.of(2024, 01, 01),
			LocalDate.of(2024, 01, 01)
		);

		when(ninjaQuotesApi.retrieveQuoteByCategory(anyString()))
			.thenReturn("HappinessText");

		final String result = phraseService.retrieveFunnyPhraseByParams(phrase);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals("HappinessText", result);

		verify(phraseRepository, times(1)).save(any(Phrase.class));
		verify(ninjaQuotesApi, times(1)).retrieveQuoteByCategory(anyString());
	}

	@Test
	public void testRetrieveFunnyPhraseWithCategoryAndCharacter() {
		final Phrase phrase = new Phrase(
			1L,
			"",
			"yoda",
			"happiness",
			LocalDate.of(2024, 01, 01),
			LocalDate.of(2024, 01, 01)
		);

		when(ninjaQuotesApi.retrieveQuoteByCategory(anyString()))
			.thenReturn("HappinessText");

		when(funTranslationApi.retrievePhraseByCharacter(anyString(), anyString()))
			.thenReturn("yodaHappinessText");

		final String result = phraseService.retrieveFunnyPhraseByParams(phrase);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals("yodaHappinessText", result);

		verify(phraseRepository, times(1)).save(any(Phrase.class));
		verify(ninjaQuotesApi, times(1)).retrieveQuoteByCategory(anyString());
		verify(funTranslationApi, times(1)).retrievePhraseByCharacter(anyString(), anyString());
	}

	@Test
	public void testRetrieveFunnyPhraseWithEmptyParams() {
		final Phrase phrase = new Phrase(
			1L,
			"",
			"",
			"",
			LocalDate.of(2024, 01, 01),
			LocalDate.of(2024, 01, 01)
		);

		when(ninjaQuotesApi.retrieveRandomQuote()).thenReturn(new QuoteResponseDto(
			"randomQuote",
			"unknown",
			"random"
		));

		final String result = phraseService.retrieveFunnyPhraseByParams(phrase);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals("randomQuote", result);

		verify(phraseRepository, times(1)).save(any(Phrase.class));
		verify(ninjaQuotesApi, times(1)).retrieveRandomQuote();
	}

	@Test
	public void testRetrieveFunnyPhraseWithOnlyCharacter() {
		final Phrase phrase = new Phrase(
			1L,
			"",
			"yoda",
			"",
			LocalDate.of(2024, 01, 01),
			LocalDate.of(2024, 01, 01)
		);

		when(ninjaQuotesApi.retrieveRandomQuote()).thenReturn(new QuoteResponseDto(
			"randomQuote",
			"unknown",
			"random"
		));

		when(funTranslationApi.retrievePhraseByCharacter(anyString(), anyString()))
			.thenReturn("yodaRandomQuote");

		final String result = phraseService.retrieveFunnyPhraseByParams(phrase);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals("yodaRandomQuote", result);

		verify(phraseRepository, times(1)).save(any(Phrase.class));
		verify(ninjaQuotesApi, times(1)).retrieveRandomQuote();
		verify(funTranslationApi, times(1)).retrievePhraseByCharacter(anyString(), anyString());
	}

	@Test
	public void testRetrieveAllFunnyCharacters() {
		final List<String> characterList = List.of("yoga");
		when(parameterService.retrieveAllFunnyCharacters())
			.thenReturn(characterList);

		final List<String> result = phraseService.retrieveAllFunnyCharacters();

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(characterList.size(), result.size());
		assertEquals(characterList.get(0), result.get(0));

		verify(parameterService, times(1)).retrieveAllFunnyCharacters();
	}

	@Test
	public void testRetrieveAllQuoteCategories() {
		final List<String> quoteList = List.of("god");
		when(parameterService.retrieveAllQuoteCategories())
			.thenReturn(quoteList);

		final List<String> result = phraseService.retrieveAllQuoteCategories();

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(quoteList.size(), result.size());
		assertEquals(quoteList.get(0), result.get(0));

		verify(parameterService, times(1)).retrieveAllQuoteCategories();
	}
}