package com.example.phrasegenerator.service;


import com.example.phrasegenerator.entity.Parameter;
import com.example.phrasegenerator.exception.types.*;
import com.example.phrasegenerator.repository.ParameterRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ParameterServiceTest {

	@Mock
	ParameterRepository parameterRepositoryMock;

	@InjectMocks
	ParameterService parameterService;

	@Test
	public void testRetrieveUrlByNameSuccessfully() {
		final Parameter parameter = new Parameter(1L, "name", "type", "content");

		when(parameterRepositoryMock.findByNameAndType(anyString(), anyString()))
			.thenReturn(Optional.of(parameter));

		final String result = parameterService.retrieveUrlByName("name");

		assertNotNull(result);
		assertEquals(parameter.getContent(), result);

		verify(parameterRepositoryMock, times(1)).findByNameAndType(anyString(), anyString());
	}

	@Test
	public void testErrorWhenRetrieveUrlByName() {
		final Parameter parameter = new Parameter(1L, "name", "type", "content");

		when(parameterRepositoryMock.findByNameAndType(anyString(), anyString()))
			.thenReturn(Optional.empty());

		final RuntimeException exception = assertThrows(UrlNotFoundException.class, () -> {
			parameterService.retrieveUrlByName("name");
		});

		assertNotNull(exception);
		assertNotNull(exception.getMessage());
		assertEquals("URL not found", exception.getMessage());

		verify(parameterRepositoryMock, times(1)).findByNameAndType(anyString(), anyString());
	}

	@Test
	public void testRetrieveCharacterByNameSuccessfully() {
		final Parameter parameter = new Parameter(1L, "name", "type", "content");

		when(parameterRepositoryMock.findByNameAndType(anyString(), anyString()))
			.thenReturn(Optional.of(parameter));

		final String result = parameterService.retrieveCharacterByName("name");

		assertNotNull(result);
		assertEquals(parameter.getContent(), result);

		verify(parameterRepositoryMock, times(1)).findByNameAndType(anyString(), anyString());
	}

	@Test
	public void testErrorWhenRetrieveCharacterByName() {
		final Parameter parameter = new Parameter(1L, "name", "type", "content");

		when(parameterRepositoryMock.findByNameAndType(anyString(), anyString()))
			.thenReturn(Optional.empty());

		final RuntimeException exception = assertThrows(CharacterNotFoundException.class, () -> {
			parameterService.retrieveCharacterByName("name");
		});

		assertNotNull(exception);
		assertNotNull(exception.getMessage());
		assertEquals("Character not found", exception.getMessage());

		verify(parameterRepositoryMock, times(1)).findByNameAndType(anyString(), anyString());
	}

	@Test
	public void testRetrieveKeyByNameSuccessfully() {
		final Parameter parameter = new Parameter(1L, "name", "type", "content");

		when(parameterRepositoryMock.findByNameAndType(anyString(), anyString()))
			.thenReturn(Optional.of(parameter));

		final String result = parameterService.retrieveKeyByName("name");

		assertNotNull(result);
		assertEquals(parameter.getContent(), result);

		verify(parameterRepositoryMock, times(1)).findByNameAndType(anyString(), anyString());
	}

	@Test
	public void testErrorWhenRetrieveKeyByName() {
		final Parameter parameter = new Parameter(1L, "name", "type", "content");

		when(parameterRepositoryMock.findByNameAndType(anyString(), anyString()))
			.thenReturn(Optional.empty());

		final RuntimeException exception = assertThrows(KeyNotFoundException.class, () -> {
			parameterService.retrieveKeyByName("name");
		});

		assertNotNull(exception);
		assertNotNull(exception.getMessage());
		assertEquals("Key not found", exception.getMessage());

		verify(parameterRepositoryMock, times(1)).findByNameAndType(anyString(), anyString());
	}

	@Test
	public void testRetrieveAllFunnyCharacters() {
		final List<Parameter> parameterList = List.of(new Parameter(1L, "name", "type", "content"));
		when(parameterRepositoryMock.findAllByType(anyString()))
			.thenReturn(parameterList);

		final List<String> result = parameterService.retrieveAllFunnyCharacters();

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(parameterList.size(), result.size());
		assertEquals(parameterList.get(0).getName(), result.get(0));

		verify(parameterRepositoryMock, times(1)).findAllByType(anyString());
	}

	@Test
	public void testRetrieveAllQuoteCategories() {
		final List<Parameter> parameterList = List.of(new Parameter(1L, "name", "type", "content"));
		when(parameterRepositoryMock.findAllByType(anyString()))
			.thenReturn(parameterList);

		final List<String> result = parameterService.retrieveAllQuoteCategories();

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(parameterList.size(), result.size());
		assertEquals(parameterList.get(0).getName(), result.get(0));

		verify(parameterRepositoryMock, times(1)).findAllByType(anyString());
	}
}