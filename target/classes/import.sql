-- URL
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (1, 'fun_translation_api', 'url', 'https://api.funtranslations.com/translate/');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (2, 'ninja_quotes_api', 'url', 'https://api.api-ninjas.com/v1/quotes');

--FUN CHARACTERS
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (3, 'yoda', 'character', 'yoda.json');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (4, 'sith', 'character', 'sith.json');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (5, 'pirate', 'character', 'pirate.json');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (6, 'cockney', 'character', 'cockney.json');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (7, 'brooklyn', 'character', 'brooklyn.json');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (8, 'old_english', 'character', 'oldenglish.json');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (9, 'shakespeare', 'character', 'shakespeare.json');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (10, 'us_to_uk', 'character', 'us2uk.json');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (11, 'uk_to_us', 'character', 'uk2us.json');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (12, 'kraut', 'character', 'kraut.json');

-- QUOTE CATEGORIES
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (20, 'alone', 'category', 'alone');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (21, 'amazing', 'category', 'amazing');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (22, 'attitude', 'category', 'attitude');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (23, 'beauty', 'category', 'beauty');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (24, 'cool', 'category', 'cool');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (25, 'courage', 'category', 'courage');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (26, 'dad', 'category', 'dad');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (27, 'dreams', 'category', 'dreams');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (28, 'environmental', 'category', 'environmental');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (29, 'forgiveness', 'category', 'forgiveness');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (30, 'freedom', 'category', 'freedom');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (31, 'friendship', 'category', 'friendship');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (32, 'funny', 'category', 'funny');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (33, 'god', 'category', 'god');
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (34, 'happiness', 'category', 'happiness');

-- KEY
INSERT INTO PARAMETER_TABLE(id, name, type, content) VALUES (99, 'ninja_quotes_api', 'key', 'kWG/FaVZd43LwAQFMy0Bpw==FiAtHDMTvTbZSY4n');
