# phrase-generator-service
This service generates for you a phrase, that can be use in gift cards. 
Although we have some possibilities. 
- generate a totally random phrase, by letting the parameters "text", "category" and "character" empty.
- generate a totally random phrase, but customized by some character, by letting the parameters "text" and "category" empty.
- generate a random phrase based in some category/topic, by letting the parameters "text" and "character" empty.
- generate a random phrase based in some category/topic, but customized by some character, by letting the parameter "text" empty.
- generate a custom phrase of you, but customized by some character, by letting the parameter "category" empty.
- finally, and not so cool, retrieve your own custom phrase, by letting the parameter "category" and "character" empty.

I used in this service two external api's, one to generate a phrase and another one to customize that based in some character.
- https://api-ninjas.com/api/quotes 
- https://api.funtranslations.com/ - It is important to mention that we are limited in 10 requests per hour xD

I created a table to save some parameters, such as url, categories and characters. In the future could be included some
environment field, to prod, dev and test values. 

You will find one main Rest endpoint, which will generate your phrase based in entry params.
More information about the endpoints were included on the Swagger interface: [http://localhost:8080/swagger-ui/index.html]
To perform locally the system, you need to follow the steps on the final of this document.

I created two endpoints, "/characters" and "/categories", to bring the possibilities to the main endpoint.

### Software Engineer
- Deorgenes Xavier Junior
- deorgenes.junior@gmail.com

### Technology stack:
- Programming language: Java [17]
- Framework: Spring-boot (3.3.1)
- Webserver: Embedded TomCat
- JDK: jdk-17
- H2 database to store phrases and parameters

### Perform Locally

1. Enter inside phrase-generator package
2. Open a terminal (Git bash for example)
3. perform this command: mvn clean spring-boot:run OR
4. perform this command: java -jar target/phrase-generator-0.0.1-SNAPSHOT.jar
5. After running is possible to access the endpoints by [http://localhost:8080/swagger-ui/index.html]
6. To stop the system is just a simple ctrl^c